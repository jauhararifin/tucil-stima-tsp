package com.github.jauhararifin.tkstima3.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 4/2/2017.
 */
public class Matrix<T> {

    private int nRow;
    private int nCol;

    List<List<T>> data;

    public Matrix(int nRow, int nCol) {
        this.nRow = nRow;
        this.nCol = nCol;

        data = new ArrayList<>();
        for (int i = 0; i < this.nRow; i++) {
            List<T> colList = new ArrayList<T>();
            for (int j = 0; j < this.nCol; j++) {
                colList.add(null);
            }
            data.set(i, colList);
        }
    }

    public int getHeight() {
        return nRow;
    }

    public int getWidth() {
        return nCol;
    }

    public T get(int row, int col) {
        return data.get(row).get(col);
    }

    public void set(int row, int col, T value) {
        data.get(row).set(col, value);
    }

    public Matrix<T> clone() {
        Matrix<T> newMatriks = new Matrix<>(nRow, nCol);
        for (int i = 0; i < nRow; i++) {
            for (int j = 0; j < nCol; j++) {
                newMatriks.set(i,j,get(i,j));
            }
        }
        return newMatriks;
    }

}
